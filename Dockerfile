FROM python:3.9.1-slim-buster

CMD ["bash"]
WORKDIR /root

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        picocom \
        vim \
    && \
    rm -rf /var/lib/apt/lists/*

COPY build/ build/
RUN cd build/ && \
    pip3 install -r requirements.txt -c constraints.txt && \
    rm -rf /root/.cache/pip

COPY *.py *.md ./
