Modbus test image
=================

Build
-----
```
docker build --tag modbus-test-image https://gitlab.com/AngeAnalytics/modbus-test-image.git
```

Run
---
```
docker run --rm -it --device /dev/rs485:/dev/rs485:rwm modbus-test-image
```

Listen on serial port
---------------------
```
timeout 5s picocom -b 19200 /dev/rs485
```

Read from modbus
----------------
```
pymodbus.console serial --port=/dev/rs485
client.read_holding_registers unit=1 address=1 count=1
```

```
./read-modbus.py
```
