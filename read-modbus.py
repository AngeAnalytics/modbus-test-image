#!/usr/bin/env python3

import logging
from pymodbus.client.sync import ModbusSerialClient as ModbusClient

log = logging.getLogger()
FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT, level=0)

client = ModbusClient(method='rtu', port='/dev/rs485', timeout=1, baudrate=9600)
client.connect()

unit = 1
log.info("Read from unit %s", unit)
read_result = client.read_holding_registers(0, 1, unit=unit)
log.info("Result: %s", read_result)
registers = read_result.registers  # Can lead to "AttributeError: 'ModbusIOException' object has no attribute 'registers'"
log.info("Registers: %s", registers);
